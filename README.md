# Preinit
Minetest mod to run code before any mod loads.

## Usage
To use preinit in your mod simply create a preinit.lua file in your mod's directory.
Preinit will run those files from every mod has them, in alphabetical modname order.

## Caveats
Preinit relies on undefined and undocumented behavior in minetest's mod load order to load before other mods.
It uses dirty hacks to check if any mods have loaded before it, and crashes the game if it detects that.
Load order in Minetest depends on dependencies and mod location.
Mods are loaded after their dependencies (i think mods with dependencies also load after all mods without dependencies).
Apart from that, mods are split into 3 categories based on their location.
User mods (configured in world.mt) are loaded first, followed by worldmods and then game mods.
In-category load order depends on filesystem structure: it seems to be sorted in reverse alphabetical order.

Due to this, preinit's directory name must start with zzzz in the top-level of the mod directory for it to work, and if any user mods are enabled it has to be installed as an user mod. If used in a game, rename the mod to something else (e.g. zzzz_game_preinit), otherwise minetest will use game load order to load the mod even if it's installed as a user mod, and users won't be able to install mods.
