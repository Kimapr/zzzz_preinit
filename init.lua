do local fn=function()-- <PREINIT>
if ____preinit_succ____ then
	minetest.after(0,function()____preinit_succ____=nil;end)
	return
end -- another preinit mod exists
if INIT~="game" then
	return -- we are not game
end
local modnms=minetest.get_modnames()
if core.get_current_modname()~="*builtin*" then
	local modfns={}
	local emods={}
	local crashenv={}
	local crashkey={}
	local crashfn=function()
		error(crashkey)
	end
	setmetatable(crashenv,{__index=crashfn,__newindex=crashfn})
	for k,v in pairs(modnms) do
		local modfn,err=loadfile(minetest.get_modpath(v).."/init.lua")
		setfenv(modfn,crashenv)
		if modfn then
			local ok,err=pcall(modfn)
			if not ok and err==crashkey then
				emods[v]=true
			end
		end
	end
	local ecmods={}
	local getmodname=minetest.get_current_modname
	local function record()
		local modname=getmodname()
		if modname then
			ecmods[modname]=true
		end
	end
	local _G=_G
	local newG,mt={},{}
	setmetatable(newG,mt)
	local rg,rs,gm,sm=rawget,rawset
	local sf,gf=setfenv,getfenv
	function rawget(t,k)
		if t==_G then
			record()
		end
		return rg(t,k)
	end
	function rawset(t,k,v)
		if t==_G then
			record()
		end
		return rs(t,k,v)
	end
	function setfenv(f,e)
		if e==_G then
			e=newG
		end
		return sf(f,e)
	end
	function getfenv(f)
		local e=gf(f)
		if e==newG then
			e=_G
		end
		return e
	end
	mt.__index=function(self,k)
		record()
		return _G[k]
	end
	mt.__newindex=function(self,k,v)
		record()
		_G[k]=v
	end
	sf(0,newG)
	local errstr=[[
	Uh oh! The mods:
	%s
	have loaded before the preinit mod!
	- what?
	Preinit is a mod that allows other mods to run code before any mods loaded.
	To do this, it makes sure it loads first. Because Minetest doesn't offer any mechanisms to force a mod to run before other mods, and mod load order is undefined, this mod relies on dirty hacks to function. These hacks have failed (or some mod messed with the detection mechanism).
	- how to fix?
	Several ways:
	* Copy&paste ]]..minetest.get_modpath(minetest.get_current_modname())..[[/init.lua to the end of ]]..minetest.get_builtin_path()..[[init.lua
	* Put one of these mods in the optional dependencies of the above-mentioned mods:
	%s
	* If you're installing mods to a game that contains preinit, try moving/copying preinit to user mods.
	* Make sure preinit is on the top level of the mods directory, not in a modpack, and its directory name starts with zzzz
	]]
	minetest.register_on_mods_loaded(function()
		local errors={}
		local goods={}
		for k,v in pairs(emods) do
			if not ecmods[k] then
				errors[#errors+1]=k
			else
				goods[#goods+1]=k
			end
		end
		if #errors>0 then
			table.sort(errors)
			table.sort(goods)
			errors=table.concat(errors,", ")
			goods=table.concat(goods,", ")
			error(errstr:format(errors,goods))
		end
	end)
end

local preinittime=true
local its={"register_","override_","unregister_","clear_"}
for nm,fn in pairs(minetest) do
	local tt=false
	for _,su in pairs(its) do
		if nm:sub(1,#su)==su then
			tt=true
			break
		end
	end
	if tt then
		minetest[nm]=function(...)
			if preinittime then error("Can't use minetest."..nm.." during preinit") end
			return fn(...)
		end
	end
end
for _,v in ipairs(modnms) do
	local path=minetest.get_modpath(v).."/preinit.lua"
	local file=io.open(path)
	if file then
		file:close()
		assert(loadfile(path))()
	end
end
preinittime=false

end fn() end -- </PREINIT>
